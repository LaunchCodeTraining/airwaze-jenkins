package com.launchcode.gisdevops;

import com.launchcode.gisdevops.data.AirportRepository;
import com.launchcode.gisdevops.features.WktHelper;
import com.launchcode.gisdevops.models.Airport;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@IntegrationTestConfig
public class AirportControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private AirportRepository airportRepository;

    @Before
    public void setup(){
        airportRepository.deleteAll();
    }

    @After
    public void tearDown(){
        airportRepository.deleteAll();
    }

    @Test
    public void airportPathWorks() throws Exception {
        this.mockMvc.perform(get("/airport/")).andExpect(status().isOk());
    }

    @Test
    public void oneAirportReturnsOneElement() throws Exception {
        Airport airport = airportRepository.save(new Airport(WktHelper.wktToGeometry("POINT(13.767200469970703 51.1328010559082)"), 1, "STL", "Lambert", "St. Louis", "STLL", 500, "CST","USA" ));
        this.mockMvc.perform(get("/airport/"))
                .andExpect(status().isOk())
                .andExpect(jsonPath(("$.features"), hasSize(1)));
    }

    @Test
    public void noAirportsMeansEmptyResults() throws Exception {
        this.mockMvc.perform(get("/airport/"))
                .andExpect(status().isOk())
                .andExpect(jsonPath(("$.type"), containsString("FeatureCollection")))
                .andExpect(jsonPath(("$.features"), hasSize(0)));
    }

    @Test
    public void allAttributesAreOnTheGeoJSON() throws Exception {
        Airport airport = airportRepository.save(new Airport(WktHelper.wktToGeometry("POINT(13.767200469970703 51.1328010559082)"), 1, "STL", "Lambert", "St. Louis", "STLL", 500, "CST" ,"USA"));
        this.mockMvc.perform(get("/airport/"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.features[0].properties.airportId", equalTo(1)))
                .andExpect(jsonPath("$.features[0].properties.faaCode", containsString("STL")))
                .andExpect(jsonPath("$.features[0].properties.city", containsString("St. Louis")))
                .andExpect(jsonPath("$.features[0].properties.name", containsString("Lambert")))
                .andExpect(jsonPath("$.features[0].properties.icao", containsString("STLL")))
                .andExpect(jsonPath("$.features[0].properties.timeZone", containsString("CST")))
                .andExpect(jsonPath("$.features[0].properties.altitude", equalTo(500)))
                .andExpect(jsonPath("$.features[0].geometry.type", equalTo("Point")))
                .andExpect(jsonPath("$.features[0].geometry.coordinates[0]", closeTo(13.767200469970703, .00001 )))
                .andExpect(jsonPath("$.features[0].geometry.coordinates[1]", closeTo(51.1328010559082, .00001)));

    }

    @Test
    public void multipleAirportsGivesMultipleFeatures() throws Exception {
        Airport airport1 = airportRepository.save(new Airport(WktHelper.wktToGeometry("POINT(13.767200469970703 51.1328010559082)"),1, "STL", "Lambert", "St. Louis", "STLL", 500, "CST" ,"USA"));
        Airport airport = airportRepository.save(new Airport(WktHelper.wktToGeometry("POINT(13.767200469970703 51.1328010559082)"), 1, "STL", "Lambert", "St. Louis", "STLL", 500, "CST" ,"USA"));
        this.mockMvc.perform(get("/airport/"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.features", hasSize(2)));
    }

    @Test
    public void searchByAirportFaaCode() throws Exception {
        Airport airport1 = airportRepository.save(new Airport(WktHelper.wktToGeometry("POINT(13.767200469970703 51.1328010559082)"),1, "STL", "Lambert", "St. Louis", "STLL", 500, "CST" ,"USA"));
        Airport airport = airportRepository.save(new Airport(WktHelper.wktToGeometry("POINT(13.767200469970703 51.1328010559082)"), 1, "LAX", "Lambert", "St. Louis", "STLL", 500, "CST" ,"USA"));
        this.mockMvc.perform(get("/airport/?faaCode=STL"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.features", hasSize(1)))
                .andExpect(jsonPath("$.features[0].properties.faaCode", equalTo("STL")));
    }
}