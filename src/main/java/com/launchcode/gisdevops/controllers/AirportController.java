package com.launchcode.gisdevops.controllers;

import com.launchcode.gisdevops.models.Airport;
import com.launchcode.gisdevops.features.Feature;
import com.launchcode.gisdevops.features.FeatureCollection;
import com.launchcode.gisdevops.data.AirportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping(value = "/airport")
public class AirportController {


    @Autowired
    private AirportRepository airportRepository;

    @RequestMapping(value = "/")
    @ResponseBody
    public FeatureCollection getAirports(@RequestParam(name = "faaCode") Optional<String> faaCode)  {
        List<Airport> airports;
        if(faaCode.isPresent() && !faaCode.get().isEmpty()) {
            airports = airportRepository.findByFaaCode(faaCode.get());
        } else {
            airports = airportRepository.findAll();
        }


        if(airports.isEmpty()) { return new FeatureCollection(); }

        FeatureCollection featureCollection = new FeatureCollection();
        for (Airport airport : airports) {
            HashMap<String, Object> properties = new HashMap<>();
            properties.put("airportId", airport.getAirportId());
            properties.put("faaCode", airport.getFaaCode());
            properties.put("city", airport.getCity());
            properties.put("name", airport.getName());
            properties.put("icao", airport.getIcao());
            properties.put("timeZone", airport.getTimeZone());
            properties.put("altitude", airport.getAltitude());
            featureCollection.addFeature(new Feature(airport.getAirportLatLong(), properties));
        }
        return featureCollection;
    }
}
